package utils

import "fmt"

type JobTitle int

const (
	// Owner represents the task has been created but not started yet
	Owner JobTitle = iota + 1
	Director
	Manager
	Engineer
	Unknown
)

var toString = map[JobTitle]string{
	Owner:    "Owner",
	Director: "Director",
	Manager:  "Manager",
	Engineer: "Engineer",
}

func (s JobTitle) String() string {
	ss := toString[s]
	return ss
}

var toID = map[string]JobTitle{
	"Owner":    Owner,
	"Director": Director,
	"Manager":  Manager,
	"Engineer": Engineer,
}

func JobTitleStr(s string) (JobTitle, error) {
	ss, err := toID[s]
	if !err {
		return Unknown, fmt.Errorf("job title uncorrect")
	}
	return ss, nil
}
