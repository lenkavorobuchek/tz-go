package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/lenkavorobuchek/tz-go/pkg/models"
	"gitlab.com/lenkavorobuchek/tz-go/pkg/utils"
	"net/http"
	"strconv"
)

func (h *Handler) createFilial(c *gin.Context) {
	var input models.Filial
	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	userData, err := h.services.Filial.GetById(input.UserID)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	title, _ := utils.JobTitleStr(userData.JobTitle)

	if !(userData.OrganizationID == input.OrganizationID && (title == utils.Owner || title == utils.Director)) {
		newErrorResponse(c, http.StatusBadRequest, "the user is not the owner or director")
		return
	}

	err2 := h.services.Filial.CreateFilial(input)
	if err2 != nil {
		newErrorResponse(c, http.StatusInternalServerError, err2.Error())
		return
	}

	c.JSON(http.StatusOK, statusResponse{"ok"})
}

func (h *Handler) editFilial(c *gin.Context) {
	var input models.UpdateFilial
	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	userData, err := h.services.Filial.GetById(*input.UserID)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	infoFilial, err := h.services.Filial.GetInfoFilial(*input.ID)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	if userData.BranchID != *input.ID && userData.JobTitle != utils.Director.String() && !(userData.JobTitle == utils.Owner.String() && infoFilial.OrganizationID == userData.OrganizationID) {
		newErrorResponse(c, http.StatusBadRequest, "the user is not in a filial")
		return
	}

	err = h.services.Filial.UpdateFilial(input)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, statusResponse{"ok"})

}

func (h *Handler) getInfoFilial(c *gin.Context) {
	filialId := c.Query("id")
	if len(filialId) == 0 {
		c.JSON(http.StatusBadRequest, statusResponse{"invalid organization id param"})
		return
	}
	filID, err := strconv.Atoi(filialId)
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid filial id param")
		return
	}

	userID := c.Query("user_id")
	if len(userID) == 0 {
		c.JSON(http.StatusBadRequest, statusResponse{"invalid user id param"})
		return
	}

	idU, err := uuid.Parse(userID)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	userData, err := h.services.Filial.GetById(idU)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	infoFilial, err := h.services.Filial.GetInfoFilial(filID)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	if userData.BranchID != filID && !(userData.JobTitle == utils.Owner.String() && infoFilial.OrganizationID == userData.OrganizationID) {
		newErrorResponse(c, http.StatusBadRequest, "the user is not in a filial")
		return
	}

	infoFilial.UserID = idU
	infoFilial.ID = filID

	c.JSON(http.StatusOK, infoFilial)

}

func (h *Handler) addWorkerFilial(c *gin.Context) {
	var input models.Employee
	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	userData, err := h.services.Filial.GetById(input.AddingID)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	if !(userData.OrganizationID == input.OrganizationID && userData.JobTitle == utils.Owner.String()) && !(userData.BranchID == input.BranchID && userData.JobTitle == utils.Director.String()) {
		newErrorResponse(c, http.StatusBadRequest, "the user is not the owner or director")
		return
	}

	_, err = utils.JobTitleStr(input.JobTitle)
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	err2 := h.services.Filial.AddWorkerFilial(input)
	if err2 != nil {
		newErrorResponse(c, http.StatusInternalServerError, err2.Error())
		return
	}

	c.JSON(http.StatusOK, statusResponse{"ok"})

}
