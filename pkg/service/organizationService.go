package service

import (
	"github.com/google/uuid"
	"gitlab.com/lenkavorobuchek/tz-go/pkg/models"
	"gitlab.com/lenkavorobuchek/tz-go/pkg/repository"
)

type OrganizationService struct {
	repo repository.Organization
}

func NewOrganizationService(repo repository.Organization) *OrganizationService {
	return &OrganizationService{repo: repo}

}

func (s *OrganizationService) CreateOrganization(org models.Organization) error {
	return s.repo.CreateOrganization(org)

}

func (s *OrganizationService) UpdateOrganization(input models.UpdateOrganization) error {
	if err := input.Validate(); err != nil {
		return err
	}
	return s.repo.UpdateOrganization(input)

}

func (s *OrganizationService) GetInfoOrganization(orgID int) (models.Organization, error) {
	return s.repo.GetInfoOrganization(orgID)
}

func (s *OrganizationService) GetAccessId(userID uuid.UUID, orgID int) (bool, error) {
	return s.repo.GetAccessId(userID, orgID)
}

func (s *OrganizationService) GetFilialsOrganization(orgID int) ([]models.FilialsInOrg, error) {
	return s.repo.GetFilialsOrganization(orgID)
}
