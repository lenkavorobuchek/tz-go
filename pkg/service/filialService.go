package service

import (
	"github.com/google/uuid"
	"gitlab.com/lenkavorobuchek/tz-go/pkg/models"
	"gitlab.com/lenkavorobuchek/tz-go/pkg/repository"
)

type FilialService struct {
	repo repository.Filial
}

func NewFilialService(repo repository.Filial) *FilialService {
	return &FilialService{repo: repo}
}

func (s *FilialService) CreateFilial(input models.Filial) error {
	return s.repo.CreateFilial(input)

}

func (s *FilialService) GetById(userId uuid.UUID) (models.Employee, error) {
	return s.repo.GetById(userId)
}

func (s *FilialService) GetInfoFilial(filID int) (models.Filial, error) {
	return s.repo.GetInfoFilial(filID)
}

func (s *FilialService) UpdateFilial(input models.UpdateFilial) error {
	if err := input.ValidateUpdateFilial(); err != nil {
		return err
	}
	return s.repo.UpdateFilial(input)
}

func (s *FilialService) AddWorkerFilial(input models.Employee) error {
	return s.repo.AddWorkerFilial(input)
}
