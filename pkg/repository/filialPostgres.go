package repository

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"gitlab.com/lenkavorobuchek/tz-go/pkg/models"
	"gitlab.com/lenkavorobuchek/tz-go/pkg/utils"
	"strings"
	"time"
)

type FilialPostgres struct {
	db *sqlx.DB
}

func NewFilialPostgres(db *sqlx.DB) *FilialPostgres {
	return &FilialPostgres{db: db}
}

func (r *FilialPostgres) CreateFilial(input models.Filial) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}
	var id int
	idDirZero := new(uuid.UUID)
	query := fmt.Sprintf("INSERT INTO %s (name_filial, country, city, address, type_filial, numbers_filial, email, photos_filial, organization_id, director_id, created_at) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) RETURNING id", filialTable)
	row := tx.QueryRow(query, input.NameFilial, input.Country, input.City, input.Address, input.TypeFilial, input.Phones, input.Emails, input.Photos, input.OrganizationID, idDirZero, time.Now())
	if err := row.Scan(&id); err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit()

}

func (r *FilialPostgres) GetById(userId uuid.UUID) (models.Employee, error) {
	var emplList models.Employee

	query := fmt.Sprintf("SELECT id_profile, job_title, email, organization_id, CASE WHEN branch_id IS NULL THEN 0 ELSE branch_id END AS branch_id FROM %s WHERE id_profile = $1", employeesTable)
	err := r.db.Get(&emplList, query, userId)

	return emplList, err
}

func (r *FilialPostgres) GetInfoFilial(filID int) (models.Filial, error) {
	var filInfo models.Filial
	var str1, str2, str3 string

	query := fmt.Sprintf("SELECT name_filial, country, city, address, type_filial, numbers_filial, email, photos_filial, organization_id FROM %s WHERE id = $1", filialTable)
	//err := r.db.Get(&filInfo, query, filID)
	row := r.db.QueryRow(query, filID)
	err := row.Scan(&filInfo.NameFilial, &filInfo.Country, &filInfo.City, &filInfo.Address, &filInfo.TypeFilial, &str1, &str2, &str3, &filInfo.OrganizationID)
	if err == nil && len(str1) != 0 && len(str2) != 0 && len(str3) != 0 {
		filInfo.Phones = strings.Split(strings.ReplaceAll(str1[1:], "}", ""), ",")
		filInfo.Emails = strings.Split(strings.ReplaceAll(str2[1:], "}", ""), ",")
		filInfo.Photos = strings.Split(strings.ReplaceAll(str3[1:], "}", ""), ",")
	}
	return filInfo, err

}

func (r *FilialPostgres) UpdateFilial(input models.UpdateFilial) error {
	setValues := make([]string, 0)
	args := make([]interface{}, 0)
	argId := 1
	if input.NameFilial != nil {
		setValues = append(setValues, fmt.Sprintf("name_filial=$%d", argId))
		args = append(args, *input.NameFilial)
		argId++
	}
	if input.Country != nil {
		setValues = append(setValues, fmt.Sprintf("country=$%d", argId))
		args = append(args, *input.Country)
		argId++
	}
	if input.City != nil {
		setValues = append(setValues, fmt.Sprintf("city=$%d", argId))
		args = append(args, *input.City)
		argId++
	}
	if input.Address != nil {
		setValues = append(setValues, fmt.Sprintf("address=$%d", argId))
		args = append(args, *input.Address)
		argId++
	}
	if input.TypeFilial != nil {
		setValues = append(setValues, fmt.Sprintf("type_filial=$%d", argId))
		args = append(args, *input.TypeFilial)
		argId++
	}
	if input.Phones != nil {
		setValues = append(setValues, fmt.Sprintf("numbers_filial=$%d", argId))
		args = append(args, *input.Phones)
		argId++
	}
	if input.Emails != nil {
		setValues = append(setValues, fmt.Sprintf("email=$%d", argId))
		args = append(args, *input.Emails)
		argId++
	}
	if input.Photos != nil {
		setValues = append(setValues, fmt.Sprintf("photos_filial=$%d", argId))
		args = append(args, *input.Photos)
		argId++
	}
	setQuery := strings.Join(setValues, ", ")
	query := fmt.Sprintf("UPDATE %s SET %s WHERE id = $%d", filialTable, setQuery, argId)
	args = append(args, input.ID)
	logrus.Debugf("updateQuerty: %s", query)
	logrus.Debugf("args: %s", args)

	result, err := r.db.Exec(query, args...)
	numbRow, _ := result.RowsAffected()
	if numbRow == 0 {
		return fmt.Errorf("uncorrected id of filial")
	}
	return err
}

func (r *FilialPostgres) AddWorkerFilial(input models.Employee) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}
	query := fmt.Sprintf("INSERT INTO %s (id_profile, job_title, organization_id, email, email_subscription, branch_id) values ($1, $2, $3, $4, $5, $6)", employeesTable)
	_, err = tx.Exec(query, input.IDProfile, input.JobTitle, input.OrganizationID, input.Email, false, input.BranchID)
	if err != nil {
		tx.Rollback()
		return err
	}

	if input.JobTitle == utils.Director.String() {
		que2 := fmt.Sprintf("UPDATE %s SET director_id = $1 WHERE id = $2", filialTable)
		_, err = tx.Exec(que2, input.IDProfile, input.BranchID)
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	return tx.Commit()
}
