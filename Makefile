build:
	docker-compose build tz-go

run:
	docker-compose up tz-go

migrate:
	migrate -path ./schema -database 'postgres://postgres:Qwerty12345@0.0.0.0:5436/postgres?sslmode=disable' up
