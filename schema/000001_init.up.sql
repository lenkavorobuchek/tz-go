
CREATE TABLE organizations
(
    id SERIAL PRIMARY KEY,
    name_organizations VARCHAR(255) NOT NULL,
    address VARCHAR(255) NOT NULL,
    type_legal_entity VARCHAR(255) NOT NULL,
    inn VARCHAR(12) NOT NULL,
    owner_id UUID,
    created_at TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE branches
(
    id SERIAL PRIMARY KEY,
    name_filial VARCHAR(255) NOT NULL,
    country VARCHAR(255) NOT NULL,
    city VARCHAR(255) NOT NULL,
    address VARCHAR(255) NOT NULL,
    type_filial VARCHAR(255) NOT NULL,
    numbers_filial TEXT[],
    email TEXT[],
    photos_filial TEXT[],
    organization_id INT REFERENCES organizations (id) ON DELETE CASCADE,
    director_id UUID,
    created_at TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE employees
(
    id SERIAL PRIMARY KEY,
    id_profile UUID NOT NULL,
    job_title VARCHAR(255) NOT NULL,
    email VARCHAR(255),
    email_subscription BOOLEAN,
    organization_id INT REFERENCES organizations (id) ON DELETE CASCADE,
    branch_id INT REFERENCES branches (id) ON DELETE CASCADE,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);






