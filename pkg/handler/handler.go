package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/lenkavorobuchek/tz-go/pkg/service"
)

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler {
	return &Handler{services: services}

}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.New()
	organization := router.Group("/organization")
	{
		organization.POST("/create", h.createOrganization)
		organization.PUT("/edit", h.editOrganization)
		organization.GET("/get_info", h.getInfoOrganization)
		organization.GET("/filials", h.getFilialsOrganization)

	}

	filial := router.Group("/filial")
	{
		filial.POST("/create", h.createFilial)
		filial.PUT("/edit", h.editFilial)
		filial.GET("/get_info", h.getInfoFilial)
		filial.POST("/add_worker", h.addWorkerFilial)

	}

	return router
}
