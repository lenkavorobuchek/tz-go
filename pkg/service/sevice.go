package service

import (
	"github.com/google/uuid"
	"gitlab.com/lenkavorobuchek/tz-go/pkg/models"
	"gitlab.com/lenkavorobuchek/tz-go/pkg/repository"
)

type Organization interface {
	CreateOrganization(org models.Organization) error
	GetInfoOrganization(orgID int) (models.Organization, error)
	UpdateOrganization(input models.UpdateOrganization) error
	GetAccessId(userID uuid.UUID, orgID int) (bool, error)
	GetFilialsOrganization(orgID int) ([]models.FilialsInOrg, error)
}

type Filial interface {
	CreateFilial(input models.Filial) error
	GetById(userId uuid.UUID) (models.Employee, error)
	GetInfoFilial(filID int) (models.Filial, error)
	UpdateFilial(input models.UpdateFilial) error
	AddWorkerFilial(input models.Employee) error
}

type Service struct {
	Organization
	Filial
}

func NewService(repos *repository.Repository) *Service {
	return &Service{
		Organization: NewOrganizationService(repos.Organization),
		Filial:       NewFilialService(repos.Filial),
	}
}
