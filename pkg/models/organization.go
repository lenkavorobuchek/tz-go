package models

import (
	"errors"
	"github.com/google/uuid"
)

type Organization struct {
	ID               int       `json:"id" db:"id"`
	NameOrganization string    `json:"name_organization" db:"name_organizations" binding:"required"`
	LegalEntity      string    `json:"legal_entity" db:"type_legal_entity" binding:"required"`
	Address          string    `json:"address" db:"address" binding:"required"`
	INN              string    `json:"inn" db:"inn" binding:"required"`
	OwnerID          uuid.UUID `json:"owner_id" db:"owner_id" binding:"required"`
	EmailOwner       string    `json:"email_owner" db:"email_owner" binding:"required,email"`
}

type FilialsInOrg struct {
	ID         int    `json:"id" db:"id"`
	NameFilial string `json:"name_filial" db:"name_filial"`
}

type UpdateOrganization struct {
	UserID           *uuid.UUID `json:"user_id"`
	NameOrganization *string    `json:"name_organization"`
	LegalEntity      *string    `json:"legal_entity"`
	Address          *string    `json:"address"`
}

func (i UpdateOrganization) Validate() error {
	if i.UserID == nil && i.NameOrganization == nil && i.LegalEntity == nil && i.Address == nil {
		return errors.New("update structure has no values")
	}
	return nil
}
