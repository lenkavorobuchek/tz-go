package repository

import (
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/lenkavorobuchek/tz-go/pkg/models"
)

type Organization interface {
	CreateOrganization(org models.Organization) error
	UpdateOrganization(input models.UpdateOrganization) error
	GetInfoOrganization(orgID int) (models.Organization, error)
	GetAccessId(userID uuid.UUID, orgID int) (bool, error)
	GetFilialsOrganization(orgID int) ([]models.FilialsInOrg, error)
}

type Filial interface {
	CreateFilial(input models.Filial) error
	GetById(userId uuid.UUID) (models.Employee, error)
	GetInfoFilial(filID int) (models.Filial, error)
	UpdateFilial(input models.UpdateFilial) error
	AddWorkerFilial(input models.Employee) error
}

type Repository struct {
	Organization
	Filial
}

func NewRepository(db *sqlx.DB) *Repository {
	return &Repository{
		Organization: NewOrganizationPostgres(db),
		Filial:       NewFilialPostgres(db),
	}
}
