package models

import "github.com/google/uuid"

type Employee struct {
	AddingID       uuid.UUID `json:"adding_id" binding:"required"`
	IDProfile      uuid.UUID `json:"id_profile" db:"id_profile" binding:"required"`
	JobTitle       string    `json:"job_title" db:"job_title" binding:"required"`
	OrganizationID int       `json:"organization_id" db:"organization_id" binding:"required"`
	BranchID       int       `json:"branch_id" db:"branch_id"`
	Email          string    `json:"email" db:"email" binding:"required,email"`
}
