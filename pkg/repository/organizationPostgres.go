package repository

import (
	"database/sql"
	"fmt"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"gitlab.com/lenkavorobuchek/tz-go/pkg/models"
	"gitlab.com/lenkavorobuchek/tz-go/pkg/utils"
	"strings"
	"time"
)

type OrganizationPostgres struct {
	db *sqlx.DB
}

func NewOrganizationPostgres(db *sqlx.DB) *OrganizationPostgres {
	return &OrganizationPostgres{db: db}
}

func (r *OrganizationPostgres) CreateOrganization(org models.Organization) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}
	var id int
	query := fmt.Sprintf("INSERT INTO %s (name_organizations, address, type_legal_entity, inn, owner_id, created_at) values ($1, $2, $3, $4, $5, $6) RETURNING id", organizationsTable)
	row := tx.QueryRow(query, org.NameOrganization, org.Address, org.LegalEntity, org.INN, org.OwnerID, time.Now())
	if err := row.Scan(&id); err != nil {
		tx.Rollback()
		return err
	}

	jobTitleT := utils.Owner.String()

	createOwnerOrg := fmt.Sprintf("INSERT INTO %s (id_profile, job_title, organization_id, email, email_subscription) values ($1, $2, $3, $4, $5)", employeesTable)

	_, err = tx.Exec(createOwnerOrg, org.OwnerID, jobTitleT, id, org.EmailOwner, false)
	if err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit()
}

func (r *OrganizationPostgres) UpdateOrganization(input models.UpdateOrganization) error {
	setValues := make([]string, 0)
	args := make([]interface{}, 0)
	argId := 1
	if input.NameOrganization != nil {
		setValues = append(setValues, fmt.Sprintf("name_organizations=$%d", argId))
		args = append(args, *input.NameOrganization)
		argId++
	}
	if input.LegalEntity != nil {
		setValues = append(setValues, fmt.Sprintf("type_legal_entity=$%d", argId))
		args = append(args, *input.LegalEntity)
		argId++
	}
	if input.Address != nil {
		setValues = append(setValues, fmt.Sprintf("address=$%d", argId))
		args = append(args, *input.Address)
		argId++
	}
	setQuery := strings.Join(setValues, ", ")
	query := fmt.Sprintf("UPDATE %s SET %s WHERE owner_id = $%d", organizationsTable, setQuery, argId)
	args = append(args, input.UserID)
	logrus.Debugf("updateQuerty: %s", query)
	logrus.Debugf("args: %s", args)

	result, err := r.db.Exec(query, args...)
	numbRow, _ := result.RowsAffected()
	if numbRow == 0 {
		return fmt.Errorf("the user does not owner organization. Can't update data")
	}
	return err
}

func (r *OrganizationPostgres) GetInfoOrganization(orgID int) (models.Organization, error) {
	var listOrg models.Organization

	query := fmt.Sprintf("SELECT name_organizations, address, type_legal_entity, inn FROM %s WHERE id = $1", organizationsTable)
	err := r.db.Get(&listOrg, query, orgID)
	return listOrg, err
}

func (r *OrganizationPostgres) GetAccessId(userID uuid.UUID, orgID int) (bool, error) {
	var id int
	countRow := fmt.Sprintf("SELECT id FROM %s WHERE id_profile = $1 AND organization_id = $2", employeesTable)
	row := r.db.QueryRow(countRow, userID, orgID)
	err := row.Scan(&id)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, fmt.Errorf("the user does not belong to the organization %d", orgID)
		}
		return false, err
	}
	return true, nil
}

func (r *OrganizationPostgres) GetFilialsOrganization(orgID int) ([]models.FilialsInOrg, error) {
	var lists []models.FilialsInOrg

	query := fmt.Sprintf("SELECT tl.id, tl.name_filial FROM %s tl INNER JOIN %s ul on tl.organization_id = ul.id WHERE ul.id = $1",
		filialTable, organizationsTable)
	err := r.db.Select(&lists, query, orgID)

	return lists, err

}
