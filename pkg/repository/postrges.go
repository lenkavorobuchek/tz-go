package repository

import (
	"database/sql"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/stdlib"
	"github.com/jmoiron/sqlx"
	"time"
)

const (
	organizationsTable = "organizations"
	filialTable        = "branches"
	employeesTable     = "employees"
)

type Config struct {
	Host     string
	Port     string
	Username string
	Password string
	DBName   string
	SSLMode  string
}

func (db *Config) GetConnectionString() string {
	return fmt.Sprintf("user=%s password=%s host=%s port=%s dbname=%s sslmode=disable",
		db.Username, db.Password, db.Host, db.Port, db.DBName)
}

func NewPostgresDB(cfg Config) (*sqlx.DB, error) {
	// parse connection string
	dbConf, err := pgx.ParseConfig(cfg.GetConnectionString())
	if err != nil {
		return nil, errors.New("failed to parse config")
	}

	dbConf.Host = cfg.Host

	//register pgx conn
	dsn := stdlib.RegisterConnConfig(dbConf)

	sql.Register("wrapper", stdlib.GetDefaultDriver())
	wdb, err := sql.Open("wrapper", dsn)
	if err != nil {
		return nil, errors.New("failed to connect to database")
	}

	const (
		maxOpenConns    = 50
		maxIdleConns    = 50
		connMaxLifetime = 3
		connMaxIdleTime = 5
	)
	db := sqlx.NewDb(wdb, "pgx")
	db.SetMaxOpenConns(maxOpenConns)
	db.SetMaxIdleConns(maxIdleConns)
	db.SetConnMaxLifetime(connMaxLifetime * time.Minute)
	db.SetConnMaxIdleTime(connMaxIdleTime * time.Minute)

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return db, nil

}
