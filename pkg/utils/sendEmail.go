package utils

import (
	"crypto/tls"
	"fmt"
	"github.com/spf13/viper"
	"gitlab.com/lenkavorobuchek/tz-go/pkg/models"
	"gopkg.in/gomail.v2"
)

func ForgotPassword(data *models.Employee, email string) error {
	msg := gomail.NewMessage()
	msg.SetHeader("From", "petland23@mail.ru")
	msg.SetHeader("To", email)
	msg.SetHeader("Subject", "Verification code")
	msg.SetBody("text/html", fmt.Sprintf("Приглашение вступить в организацию"))

	n := gomail.NewDialer(viper.GetString("email.host"), int(viper.GetUint("email.port")), viper.GetString("email.username"), viper.GetString("email.password"))
	n.TLSConfig = &tls.Config{InsecureSkipVerify: true}

	// Send the email
	if err := n.DialAndSend(msg); err != nil {
		return err
	}

	return nil

}
