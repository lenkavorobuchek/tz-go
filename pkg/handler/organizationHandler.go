package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/lenkavorobuchek/tz-go/pkg/models"
	"net/http"
	"strconv"
)

func (h *Handler) createOrganization(c *gin.Context) {
	var input models.Organization
	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	err := h.services.Organization.CreateOrganization(input)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, statusResponse{"ok"})

}

func (h *Handler) editOrganization(c *gin.Context) {
	var input models.UpdateOrganization
	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}
	err := h.services.Organization.UpdateOrganization(input)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, statusResponse{"ok"})

}

func (h *Handler) getInfoOrganization(c *gin.Context) {
	type orgResponse struct {
		NameOrganization string `json:"name_organization"`
		LegalEntity      string `json:"legal_entity"`
		Address          string `json:"address"`
		INN              string `json:"inn"`
	}
	organizationID := c.Query("id")
	if len(organizationID) == 0 {
		c.JSON(http.StatusBadRequest, statusResponse{"invalid organization id param"})
		return
	}
	orgID, err := strconv.Atoi(organizationID)
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid id param")
		return
	}

	userID := c.Query("user_id")
	if len(userID) == 0 {
		c.JSON(http.StatusBadRequest, statusResponse{"invalid user id param"})
		return
	}

	idU, err := uuid.Parse(userID)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}
	//проверка на права юзера
	_, err2 := h.services.Organization.GetAccessId(idU, orgID)
	if err2 != nil {
		newErrorResponse(c, http.StatusInternalServerError, err2.Error())
		return
	}

	info, err := h.services.Organization.GetInfoOrganization(orgID)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	resp := orgResponse{
		NameOrganization: info.NameOrganization,
		LegalEntity:      info.LegalEntity,
		Address:          info.Address,
		INN:              info.INN,
	}

	c.JSON(http.StatusOK, resp)

}

type getFilialsInOrg struct {
	Data []models.FilialsInOrg `json:"data"`
}

func (h *Handler) getFilialsOrganization(c *gin.Context) {
	organizationID := c.Query("id")
	if len(organizationID) == 0 {
		c.JSON(http.StatusBadRequest, statusResponse{"invalid organization id param"})
		return
	}
	orgID, err := strconv.Atoi(organizationID)
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid id param")
		return
	}

	userID := c.Query("user_id")
	if len(userID) == 0 {
		c.JSON(http.StatusBadRequest, statusResponse{"invalid user id param"})
		return
	}

	idU, err := uuid.Parse(userID)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}
	//проверка на права юзера
	_, err2 := h.services.Organization.GetAccessId(idU, orgID)
	if err2 != nil {
		newErrorResponse(c, http.StatusInternalServerError, err2.Error())
		return
	}

	info, err := h.services.Organization.GetFilialsOrganization(orgID)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, getFilialsInOrg{
		Data: info,
	})

}
