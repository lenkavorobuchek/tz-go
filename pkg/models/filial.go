package models

import (
	"errors"
	"github.com/google/uuid"
)

type Filial struct {
	ID             int       `json:"id" db:"id"`
	NameFilial     string    `json:"name_filial" db:"name_filial" binding:"required"`
	Country        string    `json:"country" db:"country" binding:"required"`
	City           string    `json:"city" db:"city" binding:"required"`
	Address        string    `json:"address" db:"address" binding:"required"`
	TypeFilial     string    `json:"type_filial" db:"type_filial" binding:"required"`
	Phones         []string  `json:"phones" db:"numbers_filial"`
	Emails         []string  `json:"emails" db:"email"`
	Photos         []string  `json:"photos" db:"photos_filial"`
	OrganizationID int       `json:"organization_id" db:"organization_id" binding:"required"`
	UserID         uuid.UUID `json:"user_id" binding:"required"`
}

type UpdateFilial struct {
	ID         *int       `json:"id" binding:"required"`
	NameFilial *string    `json:"name_filial"`
	Country    *string    `json:"country"`
	City       *string    `json:"city"`
	Address    *string    `json:"address"`
	TypeFilial *string    `json:"type_filial"`
	Phones     *[]string  `json:"phones"`
	Emails     *[]string  `json:"emails"`
	Photos     *[]string  `json:"photos"`
	UserID     *uuid.UUID `json:"user_id" binding:"required"`
}

func (i UpdateFilial) ValidateUpdateFilial() error {
	if i.UserID == nil && i.NameFilial == nil && i.Country == nil && i.Address == nil && i.City == nil && i.TypeFilial == nil && i.Phones == nil && i.Photos == nil && i.Emails == nil {
		return errors.New("update structure has no values")
	}
	return nil
}
