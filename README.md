Реализован REST API микросервис с использованием фреймворка gin-gonic/gin, а также концепцию внедрения зависимостей.
Была подключена база данных Postgres через Docker-контейнер. Для хранения данных реализованы 3 таблицы бд organizations, branches, employees.
Также было принято решение разбить логику на 2 сервиса Organization и Filial.
Были реализованны запросы:
    organization := router.Group("/organization")
    {
    organization.POST("/create", h.createOrganization)
    organization.PUT("/edit", h.editOrganization)
    organization.GET("/get_info", h.getInfoOrganization)
    organization.GET("/filials", h.getFilialsOrganization)

	}

	filial := router.Group("/filial")
	{
		filial.POST("/create", h.createFilial)
		filial.PUT("/edit", h.editFilial)
		filial.GET("/get_info", h.getInfoFilial)
		filial.POST("/add_worker", h.addWorkerFilial)

	}

Для эндпойнта add_worker было принято решение передевать также id пользователя, который добавляет пользователя, чтобы проверить Должность пользователя
К сожалению, не успела реализовать логику с отправкой пригласительной ссылки. Успела только создать шаблонную функцию с отправкой электронного письма.